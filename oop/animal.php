<!-- // $sheep = new Animal("shaun");

// echo $sheep->name; // "shaun"
// echo $sheep->legs; // 4
// echo $sheep->cold_blooded; // "no" -->

<?php
class animal
{
    public $nama;
    public $legs = 4;
    public $cold_blooded = "no";
    
    public function __construct($name)
    {
        $this->nama = $name;
    }
}